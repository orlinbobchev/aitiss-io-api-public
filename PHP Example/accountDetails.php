<?php
require 'authorize.php';

if (!isset($_SESSION["aitissToken"])){
	echo 'Session is not set'; exit();
}

$res = $client->request('GET', 'https://io.aitiss.com/Account/details', [
	'headers' => [
		'Authorization' => $_SESSION["aitissToken"], // please note how we authorize without using "Bearer"
		'Origin' => $website, //for debuging purposes
		'Content-Type' => 'application/json'
	]
]);

echo $res->getStatusCode();
// "200"

echo $res->getHeader('content-type')[0];
// 'application/json

$authRes = $res->getBody();


$obj = json_decode($authRes);
// { ["AccountPlan"]=> string() ["SendLimit"]=> string()  ["SentEmails"]=> int() ["AccountType"]=> string() "Subscribers" ["UsedSubscribers"]=> int() ["SubscribersLimit"]=> int() } 
//OR {"error":"......"}
?>
<h1>Account Details</h1>

<ul>
	<?php
	foreach ($obj as $value) {
		echo '<li>'.$value.'</li>';
	}
	var_dump($obj);
	?>
</ul>