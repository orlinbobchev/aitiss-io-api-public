<?php
	require 'settings.php';
	session_start(); //starting session where we are going to store the token
	
	require __DIR__ . '/vendor/autoload.php';
	
	$client = new GuzzleHttp\Client();
	
	//CHECK IF OUR TOKEN STORED IN THE SESSION IS VALID
	function checkSession() { //CHECK IF SESSION TOKEN EXISTS
		global $username,$apiKey,$website,$client;
		
		$res = $client->request('GET', 'https://io.aitiss.com/testConnection/', [
			'headers' => [
				'Authorization' => $_SESSION["aitissToken"], // please note how we authorize without using "Bearer"
				'Origin' => $website, //for debuging purposes
				'Content-Type' => 'application/json'
			]
		]);
		
		$obj =  json_decode($res->getBody());
		if (isset($obj->error)) {
			return 0;
		} else {
			return 1;
		}
	}
	//
	
	//IF OUR TOKEN IS VALID KEEP USING IT, IF NOT Authorize AND GENERATE NEW ONE
	if (checkSession()==1) {
		echo 'You already have TOKEN. You can make further requests.'. PHP_EOL;
	} else {
		$res = $client->request('GET', 'https://io.aitiss.com/Authorize/', [
			'headers' => [
				'Authorization' => $username.'.'.$apiKey, // please note how we authorize without using "Bearer"
				'Origin' => $website, //for debuging purposes
				'Content-Type' => 'application/json'
			]
		]);
		
		echo $res->getStatusCode();
		// "200"
		
		echo $res->getHeader('content-type')[0];
		// 'application/json
		
		$authRes = $res->getBody();
		// {"token":"....."} OR {"error":"......"}
		
		$obj = json_decode($authRes);
		
		if (isset($obj->error))  { echo 'Error with Authorization';} else {
			echo 'Authorize successful';
			$_SESSION["aitissToken"] = $obj->token;
		}
	}
	//
	
	echo 'TOKEN: '.$_SESSION["aitissToken"];
?>
