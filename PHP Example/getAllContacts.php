<?php
require 'authorize.php';

if (!isset($_SESSION["aitissToken"])){
	echo 'Session is not set'; exit();
}

//Just to make things prettier
function pretty_dump($arr, $d=1){
    if ($d==1) echo "<pre>";    // HTML Only
    if (is_array($arr)){
        foreach($arr as $k=>$v){
            for ($i=0;$i<$d;$i++){
                echo "\t";
            }
            if (is_array($v)){
                echo $k.PHP_EOL;
                Pretty_Dump($v, $d+1);
            } else {
                echo $k."\t".$v.PHP_EOL;
            }
        }
    }
    if ($d==1) echo "</pre>";   // HTML Only
}
//end

//Get Lists
$res = $client->request('GET', 'https://io.aitiss.com/Lists/view', [
	'headers' => [
		'Authorization' => $_SESSION["aitissToken"], // please note how we authorize without using "Bearer"
		'Origin' => $website, //for debuging purposes
		'Content-Type' => 'application/json'
	],
	'query' => ['limit' => '20']
]);

$authRes = $res->getBody();

$obj = json_decode($authRes,true); //json to array here
//Get Lists END

//Get Contacts
if (isset($_POST['list'])) {
	$res = $client->request('GET', 'https://io.aitiss.com/Contacts/view', [
	'headers' => [
		'Authorization' => $_SESSION["aitissToken"], // please note how we authorize without using "Bearer"
		'Origin' => $website, //for debuging purposes
		'Content-Type' => 'application/json'
	],
	'query' => ['limit' => '20']
]);

$authRes1 = $res->getBody();

$obj1 = json_decode($authRes1);
}
//Get Contacts End
?>
<h3>Show lists</h3>

<ul>
	<?php
	pretty_dump($obj);
	echo '<br>'.'VAR DUMP : '.'<br>';
	var_dump($obj);
	?>
</ul>

<h1>Get Contacts</h1>

<form action="getAllContacts.php" method="post">
	Limit: <input type="text" name="limit"><br>
	Cursor: <input type="text" name="cursor"><br>
	List: <input type="text" name="list"><br>
	<input type="submit">
</form>

<ul>
	<?php
	foreach ($obj1 as $value1) {
		echo '<li>'.$value1.'</li>';
	}
	var_dump($obj1);
	?>
</ul>