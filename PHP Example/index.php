<a href="getAllLists.php">Get All Lists [GET]</a> <br/>
<a href="createNewList.php">Create a List [POST]</a> <br/>
<a href="updateListName.php">Update List Name [PUT]</a> <br/>
<a href="deleteList.php">Delete a List [DELETE]</a> <br/>
<br/>
<a href="getFields.php">Get All Fields [GET]</a> <br/>
<a href="createField.php">Create A Field [POST]</a> <br/>
<a href="updateField.php">Update a Field [PUT]</a> <br/>
<a href="deleteField.php">Delete a Field [DELETE]</a> <br/>
<br/>
<a href="getAllContacts.php">Get All Contacts [GET]</a> <br/>
<a href="addContact.php">Add a Contact [POST]</a> <br/>
<a href="editContact.php">Edit a Contact [PUT]</a> <br/>
<a href="unsubscribeContacts.php">Unsubscribe Contacts [PUT]</a> <br/>
<a href="deleteContacts.php">Delete Contacts [DELETE]</a> <br/>
<br/>
<a href="accountDetails.php">Get Account Details [GET]</a> <br/>
<br/>