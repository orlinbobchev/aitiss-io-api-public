Get your API key from: https://app.aitiss.com/emails

Please send your request with origin - the domain name you registered on aitiss.com.


# Aitiss-io-API-public

This is the public API of Aitiss.com for 3rd party implementations.

This API enables you to:

	Obtain Token
	Test Connection (Token validity)
	Get Account Details
	Get User Lists (+ pagination with cursor)
	Create List
	Edit List Name
	Delete a List
	
	Get Fields (Those are fields that can be used in Email Templates)
	Create Field
	Update Field
	Delete Field
	
	Get All User Contacts (You can filter by List. + pagination with cursor)
	Add Contact to List/Lists
	Edit Contact
	Unsubscribe Contact/Contacts
	Delete Contact/Contacts
	
**Obtain Token**

Type: **GET**

Address: https://io.aitiss.com/Authorize/

Expects Header:

|  |  |
|--|--|
| Content-Type | **application/json** |
| Authorization | **username.apiKey** (please note the dot) |


Returns:
Token

    {
    	"token": "**************"
    }
	
Or

    {
    	"error":"......"
    }


**Test Connection (Is Token Valid)**

Type: **GET**

Address: https://io.aitiss.com/testConnection/

Expects Header:

|  |  |
|--|--|
| Content-Type | **application/json** |
| Authorization | **token** (from authorization) |


Returns:

    {
    	"response": "Hello {USERNAME}"
    }
	
Or

    {
    	"error":"......"
    }


**Get Account Details**

Type: **GET**

Address: https://io.aitiss.com/Account/details

Expects Header:

|  |  |
|--|--|
| Content-Type | **application/json** |
| Authorization | **token** (from authorization) |


Returns:

	{
		"AccountType": "(string)",
		"UsedSubscribers": (int),
		"SubscribersLimit": (int),
		"AccountPlan": "(string)",
		"SendLimit": "(int) or (string if unlimited)",
		"SentEmails": (int)
	}
	
Or

    {
    	"error":"......"
    }

	
**Get User Lists (+ pagination with cursor)**

Type: **GET**

Address: https://io.aitiss.com/Lists/view

Expects Header:

|  |  |
|--|--|
| Content-Type | **application/json** |
| Authorization | **token** (from authorization) |

Can Have URL Parameters: {example-> https://io.aitiss.com/Lists/all?limit=300&cursor=CksSRWoYc35jcnlwdG8tYW5hbHl6ZXItMTgwNjA4ciELEgVVc2VycyIEbmV2aQwLEgVMaXN0cxiAgICAxOiBCgyiAQVVc2VycxgAIAA%3D}

|  |  |
|--|--|
| limit (int) (optional) | default 10 - max 300 |
| cursor (optional) | !needs to be URL encoded! |

Returns:

	{
		"lists": [
			{
				"contacts": (int),
				"listName": "(string)",
				"listCreationDate": "(date) example: 07.09.2019 15:46:04",
				"id": "(int)"
			},
			{
				"listCreationDate": "11.12.2018 11:17:59",
				"contacts": 10,
				"listName": "Test EM Course",
				"id": "5643418646514"
			},
			.............................
		],
		"cursor": "CksSRWoYc35jcnlwdG8tYW5hbHl6ZXItMTgwNjA4ciELEgVVc2VycyIEbmV2aQwLEgVMaXN0cxiAgNDP+/XxCwyiAQVVc2VycxgAIAA="
	}
	
Or

    {
    	"error":"......"
    }

**Create List**

Type: **POST**

Address: https://io.aitiss.com/Lists/create

Expects Header:

|  |  |
|--|--|
| Content-Type | **application/json** |
| Authorization | **token** (from authorization) |

Body Data:

|  |  |
|--|--|
| listName | "string" |

Body example: {"listName": "ioApitest1 list edit"}

Returns:

	{
		"response": "List created successfully!"
	}
	
Or

    {
    	"error":"......"
    }

**Edit List Name**

Type: **PUT**

Address: https://io.aitiss.com/Lists/update

Expects Header:

|  |  |
|--|--|
| Content-Type | **application/json** |
| Authorization | **token** (from authorization) |

Body Data:

|  |  |
|--|--|
| listName | "string" |
| oldListName | "string" |

Body example: {"listName": "ioApitest1 list edit", "oldListName": "ioApitest1 list"}

Returns:

	{
		"response": "The list is edited!"
	}
	
Or

    {
    	"error":"......"
    }

**Delete a List**

Type: **DELETE**

Address: https://io.aitiss.com/Lists/delete

Expects Header:

|  |  |
|--|--|
| Content-Type | **application/json** |
| Authorization | **token** (from authorization) |

Body Data:

|  |  |
|--|--|
| id | "int" |

Body example: {"id":5678622323769344}

Returns:

	{
		"response": "The list is deleted!"
	}
	
Or

    {
    	"error":"......"
    }

**Get Fields**
(Those are fields that can be used in Email Templates)

Type: **GET**

Address: https://io.aitiss.com/Fields/view

Expects Header:

|  |  |
|--|--|
| Content-Type | **application/json** |
| Authorization | **token** (from authorization) |


Returns (example):

	{
		"ColumnType": {
			"Birthday": "date",
			"name": "text",
			"superNum": "number",
			"Email": "text"
		},
		"Columns": [
			"Email",
			"name",
			"Birthday",
			"superNum"
		]
	}
	
Or

    {
    	"error":"......"
    }

**Create Field**

Type: **POST**

Address: https://io.aitiss.com/Fields/create

Expects Header:

|  |  |
|--|--|
| Content-Type | **application/json** |
| Authorization | **token** (from authorization) |

Body Data:

|  |  |
|--|--|
| columnName | "string" |
| columnType | "string" |

**columnType can be one of the three (text|number|date)**

Body example: {"columnName": "Buyer","columnType": "number"}

Returns:

	{
		"response": "Column created!"
	}
	
Or

    {
    	"error":"......"
    }

**Update Field**

Type: **PUT**

Address: https://io.aitiss.com/Fields/update

Expects Header:

|  |  |
|--|--|
| Content-Type | **application/json** |
| Authorization | **token** (from authorization) |

Body Data:

|  |  |
|--|--|
| columnName | "string" |
| columnType | "string" |
| oldColumnName | "string" |

**columnType can be one of the three (text|number|date)**

Body example: {"columnName": "Customer","columnType": "number","oldColumnName": "Buyer"}

Returns:

	{
		"response": "Column edited!"
	}
	
Or

    {
    	"error":"......"
    }

**Delete Field**

Type: **PUT**

Address: https://io.aitiss.com/Fields/delete

Expects Header:

|  |  |
|--|--|
| Content-Type | **application/json** |
| Authorization | **token** (from authorization) |

Body Data:

|  |  |
|--|--|
| columnName | "string" |

Body example: {"columnName": "Customer"}

Returns:

	{
		"response": "Column deleted!"
	}
	
Or

    {
    	"error":"......"
    }

**Get All User Contacts**
(You can filter by List. + pagination with cursor)

Type: **GET**

Address: https://io.aitiss.com/Contacts/view

Expects Header:

|  |  |
|--|--|
| Content-Type | **application/json** |
| Authorization | **token** (from authorization) |

Can Have URL Parameters: {example-> https://io.aitiss.com/Contacts/all?list=10kNEWimport&limit=300&cursor=CksSRWoYc35jcnlwdG8tYW5hbHl6ZXItMTgwNjA4ciELEgVVc2VycyIEbmV2aQwLEgVMaXN0cxiAgICAxOiBCgyiAQVVc2VycxgAIAA%3D}

|  |  |
|--|--|
| email (string) (optional) | You can filter by email |
| list (string) (optional) | You can filter by list name |
| limit (int) (optional) | default 10 - max 300 |
| cursor (optional) | !needs to be URL encoded! |

Returns (**example IF filtered by LIST**):

	{
		"contacts": [
			{
				"isUnsubscribed": 0,
				"name": "name",
				"dateAdded_full": 1536566122,
				"methodAdded": "Import",
				"inList": [
					"nopainnogain",
					"10kNEWimport"
				],
				"Email": "1000ws@**ail.com",
				"unsubscribedDate": ""
			},
			{
				"methodAdded": "Import",
				"Email": "1001ws@**ail.com",
				"isUnsubscribed": 0,
				"inList": [
					"nopainnogain",
					"10kNEWimport"
				],
				"name": "name",
				"dateAdded_full": 1536566122,
				"unsubscribedDate": ""
			},
			............................
		],
		"cursor": "CloSVGoYc35jcnlwdG8tYW5hbHl6ZXItMTgwNjA4cjALEgVVc2VycyIHQm9iY2hldgwLEghDb250YWN0cyIQMTAwNHdzQGdtYWlsLmNvbQyiAQVVc2VycxgAIAA="
	}
	
Or

    {
    	"error":"......"
    }

Returns (**example WITHOUT LIST FILTER**):

	{
		"contacts": [
			{
				"dateAdded_full": {
					"kk1100k": 1531920317,
					"100k import": 1532607590
				},
				"methodAdded": {
					"kk1100k": "Import",
					"100k import": "Import"
				},
				"Email": "100007203626207@**soft.co",
				"isUnsubscribed": {
					"kk1100k": 0,
					"100k import": 0
				},
				"inList": [
					"kk1100k",
					"100k import"
				],
				"name": "name"
			},
			{
				"dateAdded_full": {
					"kk1100k": 1531920548,
					"100k import": 1532607542
				},
				"methodAdded": {
					"kk1100k": "Import",
					"100k import": "Import"
				},
				"Email": "100008197280863@**soft.co",
				"isUnsubscribed": {
					"kk1100k": 0,
					"100k import": 0
				},
				"inList": [
					"kk1100k",
					"100k import"
				],
				"name": "name"
			},
			...................................
		],
		"cursor": "CmMSXWoYc35jcnlwdG8tYW5hbHl6ZXItMTgwNjA4cjkLEgVVc2VycyIHQm9iY2hldgwLEghDb250YWN0cyIZMTAwMDUxNjIzMTA2NTU1QG50c29mdC5jbwyiAQVVc2VycxgAIAA="
	}

Or

    {
    	"error":"......"
    }
	
**Add Contact**

Type: **POST**

Address: https://io.aitiss.com/Contacts/create

Expects Header:

|  |  |
|--|--|
| Content-Type | **application/json** |
| Authorization | **token** (from authorization) |

Body Data:

|  |  |
|--|--|
| email | "string" |
| custom fields (optional) | "string" |
| list (your lists need to exist)| "array" |

Body example: {"email": "donaldhilary@abv.com", "list":["list1","list2"]}

Returns:

	{
		"response": "The contact is added!"
	}
	
Or

    {
    	"error":"......"
    }
	
**Edit Contact**

Type: **PUT**

Address: https://io.aitiss.com/Contacts/update

Expects Header:

|  |  |
|--|--|
| Content-Type | **application/json** |
| Authorization | **token** (from authorization) |

Body Data:

|  |  |
|--|--|
| email (can't be edited) | "string" |
| custom fields (optional) | "string" |
| list (your lists need to exist) (optional)| "array" |

Body example: {"email": "donaldhilary@abv.com", "list":["list1","list2"]}
**Please note that your edit is absolute. If you edit contacts list and you do not include previous lists, only the one in the input will remain. List field is optional and if you do not pass it, Contact lists will left unchanged. If you plan to edit lists, a good practice is first to get the contacts lists with the Get function.**

Returns:

	{
		"response": "The contact is edited!"
	}
	
Or

    {
    	"error":"......"
    }
	
**Unsubscribe a Contact**

Type: **PUT**

Address: https://io.aitiss.com/Contacts/unsubscribe

Expects Header:

|  |  |
|--|--|
| Content-Type | **application/json** |
| Authorization | **token** (from authorization) |

Body Data:

|  |  |
|--|--|
| email | "array" |
| list (your lists need to exist) (from those you wish to unsubscribe)| "array" |

Body example: {"email": ["100007203626207@ntsoft.co"], "list":["kk1100k","nopainnogain"]}


Returns:

	{
		"response": "The selected contact/s is/are unsubscribed!"
	}
	
Or

    {
    	"error":"......"
    }
	
**Delete a Contact**

Type: **DELETE**

Address: https://io.aitiss.com/Contacts/delete

Expects Header:

|  |  |
|--|--|
| Content-Type | **application/json** |
| Authorization | **token** (from authorization) |

Body Data:

|  |  |
|--|--|
| email | "array" |
| list (the contact will be delete...)| "array" |

Body example: {"email": ["100007203626207@ntsoft.co"], "list":["kk1100k","nopainnogain"]}


Returns:

	{
		"response": "Contact/s Deleted!"
	}
	
Or

    {
    	"error":"......"
    }